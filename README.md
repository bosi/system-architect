<img src="https://gitlab.com/Bosi/system-architect/raw/master/public/assets/images/logo.png" width="400" alt="Logo">

## About System-Architect

The System-Architect is a simple tool used to get an overview over all applications a system has.

This project is inspired by some ideas we had at work and the fact that commercial enterprise architecture managment
tools, the kind of tools we need for our purposes, are really expensive. The System-Architect uses a very simple version
of ArchiMate, a enterprise architecture modelling language for describing and visualising enterprise architecture based
on a service point of view (see [reference](https://pubs.opengroup.org/architecture/archimate3-doc/) for more details).

<img src="https://gitlab.com/Bosi/system-architect/raw/master/docs/screen_overview.png" width="1000" alt="Screen Overview">

## Features

* create applications including interfaces like web-interfaces or apis
* create and manage custom properties like corresponding team or used programming language
* create user-applications like browser or client-apps
* visualise all dependencies between
* create grouped views based on properties
* cache for rendered pictures

## Requirements

If you're using docker you can find a Dockerfile inside the `docker/` directory with php, apache, java and plantuml
installed. There is also a docker-compose.yml you can use for setting up your environment. Please keep in mind that the
Dockerfile is not configured for productive purposes. Thus php will show exceptions etc.

## Installation

I will only explain the "docker"-way because it's easier and faster from my point of view. `docker` and `docker-compose`
is needed for this approach.

```bash
# clone the source code
git clone https://gitlab.com/Bosi/system-architect.git
# go into docker dir
cd docker
# adjust user-id and timezone
vim .env
# start containers
docker-compose up -d
# go into the container
docker exec -it --user=www-data system_architect_php_1 bash
# init project
composer init-laravel
# (optional) create some test data
php artisan db:seed
```

## Update

```bash
composer update-app
```

You can now access via browser:

* application: http://localhost
* phpmyadmin: http://localhost:8081

## Used Frameworks and Libraries

This project make use of several amazing libraries and frameworks:

* Laravel 9 ([link](https://laravel.com/))
* Bootstrap 4 ([link](https://getbootstrap.com/))
* Fontawesome 5 ([link](https://fontawesome.com/))
* PlantUML ([link](http://plantuml.com/en/))
* Docker/Docker-Compose ([link](https://www.docker.com/) and [link](https://docs.docker.com/compose/))

## Disclaimer

The System-Architect is still in alpha. Be aware of bug and exceptions which may occur during use.

## Todo

* more tests
* forms error handling
* user and permission management
* docker-image for production
