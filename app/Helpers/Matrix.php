<?php

namespace App\Helpers;

use App\Models\Application;
use App\Models\ApplicationProperty;
use App\Models\Property;
use App\Models\PropertyOption;
use Illuminate\Http\Request;

class Matrix
{
    public const DIMENSIONS = ['row', 'column', 'color'];

    private ?int $propIdColumn = null;
    private ?Property $propColumn = null;

    private ?int $propIdRow = null;
    private ?Property $propRow = null;

    private ?int $propIdColor = null;
    private ?Property $propColor = null;

    public function __construct(Request $request)
    {
        foreach (self::DIMENSIONS as $dimension) {
            $id = $request->get('prop_id_' . $dimension);
            if ($id !== null) {
                $this->{'propId' . ucfirst($dimension)} = $id;
                $this->{'prop' . ucfirst($dimension)} = Property::query()->findOrFail($id);
            }
        }
    }

    public static function getPropertyList(): array
    {
        $props = [null => '- none -'];
        Property::query()->each(function (Property $property) use (&$props) {
            $props[$property->id] = $property->name;
        });

        return $props;
    }

    public function getColumnValues(): array
    {
        if ($this->propIdColumn === null) {
            return [];
        }
        return $this->getPropValues($this->propIdColumn);
    }

    public function getRowValues(): array
    {
        if ($this->propIdRow === null) {
            return [];
        }
        return $this->getPropValues($this->propIdRow);
    }

    public function getColorValues(): array
    {
        return PropertyOption::query()
            ->where('property_id', $this->propIdColor)
            ->whereNotNull('color')
            ->orderBy('name')
            ->get()
            ->toArray();
    }

    public function getMatrixData(): array
    {
        $data = [];
        foreach ($this->getRowValues() as $row) {
            $data[$row] = [];
            foreach ($this->getColumnValues() as $column) {
                $data[$row][$column] = [];
            }
        }

        Application::query()
            ->where('is_user_application', false)
            ->orderBy('name')
            ->each(function (Application $application) use (&$data) {
                $columnValue = $this->getApplicationPropValue($application, 'column') ?? '';
                $rowValue = $this->getApplicationPropValue($application, 'row') ?? '';
                $color = $this->getApplicationPropColor($application);

                $data[$rowValue][$columnValue][] = [
                    'id'    => $application->id,
                    'name'  => $application->name,
                    'color' => $color,
                ];
            });

        return $data;
    }

    public function getPropColumn(): ?Property
    {
        return $this->propColumn;
    }

    public function getPropRow(): ?Property
    {
        return $this->propRow;
    }

    public function getPropColor(): ?Property
    {
        return $this->propColor;
    }

    private function getPropValues(int $propID): array
    {
        $values = [];

        ApplicationProperty::query()
            ->where('property_id', $propID)
            ->orderBy('value')
            ->each(function (ApplicationProperty $appProp) use (&$values) {
                $v = $appProp->getPumlValue();
                if (!in_array($v, $values)) {
                    $values[] = $v;
                }
            });

        $values[] = '';

        return $values;
    }

    private function getApplicationPropValue(Application $application, string $dimension): ?string
    {
        $propId = $this->{'propId' . ucfirst($dimension)};
        if ($propId === null) {
            return null;
        }

        $appProp = $application->application_properties()
            ->where('property_id', $propId)
            ->first();

        if ($appProp === null) {
            return null;
        }

        return $appProp->getPumlValue();
    }

    private function getApplicationPropColor(Application $application): ?string
    {
        if ($this->propIdColor === null) {
            return null;
        }

        $appProp = $application->application_properties()
            ->where('property_id', $this->propIdColor)
            ->first();

        if (
            $appProp === null ||
            $appProp->property_option === null ||
            empty($appProp->property_option->color)
        ) {
            return null;
        }

        return $appProp->property_option->color;
    }
}