<?php


namespace App\Helpers;


use Illuminate\Support\Str;

class MenuHelper
{
    /**
     * @param string $route
     * @return bool
     *
     * @todo test
     */
    public static function isActive(string $route): bool
    {
        if ($route === route('graphic.index')) {
            if (request()->url() === $route) {
                return true;
            }
            return Str::startsWith(request()->path(), 'puml');
        }

        return Str::startsWith(request()->url(), $route);
    }
}