<?php

namespace App\Helpers;

use App\Models\Application;
use App\Models\ApplicationProperty;
use App\Models\Property;
use Illuminate\Database\Eloquent\Collection;

class Table
{
    private Collection $properties;

    public function __construct()
    {
        $this->properties = Property::query()->orderBy('name')->get();

    }

    public function getItems(): array
    {
        $items = [];

        Application::query()
            ->where('is_user_application', false)
            ->with('application_properties')
            ->each(function (Application $application) use (&$items) {
                $appData = [
                    'Name'        => $application->name,
                    'Description' => $application->description ?? '-',
                ];

                foreach ($this->properties as $prop) {
                    /** @var ApplicationProperty $appProp */
                    $appProp = $application->application_properties->where('property_id', $prop->id)->first();
                    $appData[$prop->name] = ($appProp === null) ? '-' : $appProp->getPumlValue();
                }

                $items[] = $appData;
            });

        return $items;
    }

    public function getItemsEncoded(): string
    {
        return json_encode($this->getItems(), JSON_THROW_ON_ERROR);
    }

    public function getFields(): array
    {
        $fields = array_map(
            fn(array $prop) => ['key' => $prop['name'], 'label' => $prop['name'], 'sortable' => true],
            $this->properties->toArray()
        );

        return array_merge([
            ['key' => 'Name', 'label' => 'Name', 'sortable' => true],
            ['key' => 'Description', 'label' => 'Description', 'sortable' => true]
        ], $fields);
    }

    public function getFieldsEncoded(): string
    {
        return json_encode($this->getFields(), JSON_THROW_ON_ERROR);
    }
}