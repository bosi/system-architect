<?php

namespace App\Http\Controllers;

use App\Models\ApplicationInterface;
use App\Models\Application;
use App\Models\ApplicationProperty;
use App\Models\Property;
use App\Models\Usage;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public const VIEW_PREFIX = 'application';
    public const ROUTE_PREFIX = 'applications';
    public const TITLE_PREFIX = 'labels.titles.' . self::VIEW_PREFIX;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = Application::allSorted();
        $title = __(self::TITLE_PREFIX . '.index');
        return view(self::VIEW_PREFIX . '.index', compact('title', 'applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $applicationProperties = [];

        foreach (Property::all() as $property) {
            $applicationProperties[] = [
                'property_id' => $property->id,
                'name'        => $property->name,
                'is_enum'     => $property->is_enum,
                'options'     => $property->is_enum ? $property->getOptions() : null,
            ];
        }

        $usages = array_map(function ($appInterface) {
            return [
                'target_application_id'           => $appInterface['application_id'],
                'target_application_interface_id' => $appInterface['id'],
                'name'                            => Application::query()->find($appInterface['application_id'])->getTitle() . ': ' . $appInterface['name'],
            ];
        }, ApplicationInterface::allSorted()->toArray());

        $title = __(self::TITLE_PREFIX . '.create');
        return view(self::VIEW_PREFIX . '.createOrUpdate', compact('title', 'applicationProperties', 'usages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $application = new Application($request->only([
            'name',
            'description',
            'lifecycle_status',
            'is_user_application'
        ]));
        $application->saveOrFail();

        foreach ($request->get('applicationProperties') ?? [] as $propertyId => $propertyValue) {
            if (empty($propertyValue)) {
                continue;
            }
            $applicationProperty = new ApplicationProperty([
                'application_id' => $application->id,
                'property_id'    => $propertyId,
            ]);

            if (Property::query()->find($propertyId)->is_enum) {
                $applicationProperty->property_option_id = $propertyValue;
            } else {
                $applicationProperty->value = $propertyValue;
            }

            $applicationProperty->saveOrFail();
        }

        foreach ($request->get('usages') ?? [] as $usedInterfaceId) {
            $usage = new Usage(['application_id' => $application->id, 'application_interface_id' => $usedInterfaceId]);
            $usage->saveOrFail();
        }

        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-created', ['type' => 'Application']));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Application $application
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Application $application
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Application $application)
    {
        $applicationProperties = array_map(function ($property) use ($application) {
            $applicationProperty = ApplicationProperty::query()->where([
                'application_id' => $application->id,
                'property_id'    => $property['id'],
            ])->first();

            $propertyModel = Property::query()->find($property['id']);
            $isEnum = $propertyModel->is_enum ?? false;

            return [
                'property_id' => $property['id'],
                'name'        => $property['name'],
                'is_enum'     => $isEnum,
                'value'       => $isEnum ? $applicationProperty->property_option_id ?? '' : ($applicationProperty->value ?? ''),
                'options'     => $isEnum ? $propertyModel->getOptions() : null,
            ];

        }, Property::all()->toArray());


        $usages = array_map(function ($appInterface) use ($application) {
            return [
                'target_application_id'           => $appInterface['application_id'],
                'target_application_interface_id' => $appInterface['id'],
                'name'                            => Application::query()->find($appInterface['application_id'])->getTitle() . ': ' . $appInterface['name'],
                'value'                           => Usage::query()->where([
                    'application_id'           => $application->id,
                    'application_interface_id' => $appInterface['id']
                ])->exists(),
            ];
        }, ApplicationInterface::allSorted()->toArray());

        $title = __(self::TITLE_PREFIX . '.update');
        return view(self::VIEW_PREFIX . '.createOrUpdate',
            compact('title', 'application', 'applicationProperties', 'usages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Application  $application
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        $application->fill($request->only(['name', 'description', 'lifecycle_status']));
        $application->saveOrFail();

        foreach ($request->get('applicationProperties') ?? [] as $propertyId => $propertyValue) {
            $applicationProperty = ApplicationProperty::query()->firstOrCreate([
                'application_id' => $application->id,
                'property_id'    => $propertyId
            ]);

            if (empty($propertyValue)) {
                $applicationProperty->delete();
                continue;
            }

            if ($applicationProperty->property->is_enum) {
                $applicationProperty->fill(['value' => null, 'property_option_id' => $propertyValue]);
            } else {
                $applicationProperty->fill(['value' => $propertyValue]);
            }

            $applicationProperty->saveOrFail();
        }

        foreach ($request->get('usages') ?? [] as $usedInterfaceId) {
            $usage = Usage::query()->firstOrCreate([
                'application_id'           => $application->id,
                'application_interface_id' => $usedInterfaceId
            ]);
            $usage->saveOrFail();
        }

        /** @var Usage $usage */
        foreach (Usage::query()->where(['application_id' => $application->id])->get() as $usage) {
            if (!in_array($usage->application_interface_id, $request->get('usages') ?? [])) {
                $usage->delete();
            }
        }

        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-updated', ['type' => 'Application']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Application $application
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Application $application)
    {
        $application->delete();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-deleted', ['type' => 'Application']));
    }
}
