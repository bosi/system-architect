<?php

namespace App\Http\Controllers;

use App\Models\ApplicationInterface;
use App\Models\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApplicationInterfaceController extends Controller
{
    public const VIEW_PREFIX = 'application-interface';
    public const ROUTE_PREFIX = 'application-interfaces';
    public const TITLE_PREFIX = 'labels.titles.' . self::VIEW_PREFIX;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appInterfaces = ApplicationInterface::allSorted();
        $title = __(self::TITLE_PREFIX . '.index');
        return view(self::VIEW_PREFIX . '.index', compact('title', 'appInterfaces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $applications = [];
        foreach (Application::allSorted(Application::query()->where('is_user_application', false)) as $application) {
            $applications[$application->id] = $application->getTitle();
        }

        $title = __(self::TITLE_PREFIX . '.create');
        return view(self::VIEW_PREFIX . '.createOrUpdate', compact('title', 'applications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $appInterface = new ApplicationInterface($request->except(['_token']));
        $appInterface->saveOrFail();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-created', ['type' => 'App-Interface']));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ApplicationInterface $appInterface
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInterface $appInterface)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ApplicationInterface $applicationInterface
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(ApplicationInterface $applicationInterface)
    {
        $applications = [];
        foreach (Application::allSorted(Application::query()->where('is_user_application', false)) as $application) {
            $applications[$application->id] = $application->getTitle();
        }

        $title = __(self::TITLE_PREFIX . '.update');
        return view(self::VIEW_PREFIX . '.createOrUpdate', compact('title', 'applicationInterface', 'applications'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request         $request
     * @param \App\Models\ApplicationInterface $applicationInterface
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request, ApplicationInterface $applicationInterface)
    {
        $applicationInterface->fill($request->except(['_token']));
        $applicationInterface->saveOrFail();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-updated', ['type' => 'App-Interface']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ApplicationInterface $applicationInterface
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(ApplicationInterface $applicationInterface)
    {
        $applicationInterface->delete();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-deleted', ['type' => 'App-Interface']));
    }
}
