<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Database;
use App\Models\DatabaseConnection;
use Illuminate\Http\Request;

class DatabaseController extends Controller
{
    public const VIEW_PREFIX = 'database';
    public const ROUTE_PREFIX = 'databases';
    public const TITLE_PREFIX = 'labels.titles.' . self::VIEW_PREFIX;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $databases = Database::all();
        $title = __(self::TITLE_PREFIX . '.index');
        return view(self::VIEW_PREFIX . '.index', compact('title', 'databases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $applications = [];
        foreach (Application::allSorted() as $application) {
            $applications[$application->id] = [
                'target_application_id' => $application->id,
                'name'                  => $application->getTitle(),
            ];
        }

        $title = __(self::TITLE_PREFIX . '.create');
        return view(self::VIEW_PREFIX . '.createOrUpdate', compact('title', 'applications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $database = new Database($request->except(['_token']));
        $database->saveOrFail();

        foreach ($request->get('applications') ?? [] as $applicationId) {
            $databaseConnection = new DatabaseConnection([
                'application_id' => $applicationId,
                'database_id'    => $database->id,
            ]);
            $databaseConnection->saveOrFail();
        }

        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-created', ['type' => 'Database']));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Database $database
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Database $database)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Database $database
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Database $database)
    {
        $applications = [];
        foreach (Application::allSorted() as $application) {
            $applications[$application->id] = [
                'target_application_id' => $application->id,
                'name'                  => $application->getTitle(),
                'value'                 => DatabaseConnection::query()->where([
                    'application_id' => $application->id,
                    'database_id'    => $database->id,
                ])->exists(),
            ];
        }

        $title = __(self::TITLE_PREFIX . '.update');
        return view(self::VIEW_PREFIX . '.createOrUpdate', compact('title', 'database', 'applications'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Database     $database
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request, Database $database)
    {
        $database->fill($request->except(['_token']));
        $database->saveOrFail();

        foreach ($request->get('applications') ?? [] as $applicationId) {
            $databaseConnection = DatabaseConnection::query()->firstOrCreate([
                'application_id' => $applicationId,
                'database_id'    => $database->id,
            ]);
            $databaseConnection->saveOrFail();
        }

        /** @var DatabaseConnection $databaseConnection */
        foreach (DatabaseConnection::query()->where(['database_id' => $database->id])->get() as $databaseConnection) {
            if (!in_array($databaseConnection->application_id, $request->get('applications') ?? [])) {
                $databaseConnection->delete();
            }
        }

        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-updated', ['type' => 'Database']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Database $database
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Database $database)
    {
        $database->delete();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-deleted', ['type' => 'Database']));
    }
}
