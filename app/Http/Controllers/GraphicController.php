<?php

namespace App\Http\Controllers;

use App\Helpers\Matrix;
use App\Helpers\Table;
use App\Models\ApplicationInterface;
use App\Models\Application;
use App\Models\ApplicationProperty;
use App\Models\Database;
use App\Models\Property;
use App\Models\Usage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class GraphicController extends Controller
{
    /**
     * @param Property|null $property
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(Property $property = null)
    {
        $applications = Application::query()->where('is_user_application', false)->get();
        $userApplications = Application::query()->where('is_user_application', true)->get();
        $usages = Usage::all();
        $databases = Database::all();
        $properties = Property::query()->where('is_enum', true)->whereHas('applicationProperties')->get();

        $viewData = compact('applications', 'usages', 'databases', 'userApplications');

        $title = 'Overview';

        if ($property !== null) {
            $title .= ' (grouped by "' . $property->name . '")';
            $viewData['groupRenderProperty'] = $property;
        }

        $svg = self::generateSvgPuml('puml.overview', $viewData);

        return view('overview', compact('svg', 'title', 'properties'));
    }

    public function matrix(Request $request)
    {
        $matrix = new Matrix($request);
        $title = 'Matrix-Overview';
        return view('matrix', compact('matrix', 'title',));
    }

    public function table()
    {
        $table = new Table();
        $title = 'Table-Overview';
        return view('table', compact('title', 'table'));
    }

    /**
     * @param ApplicationInterface $interface
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function interface(ApplicationInterface $interface)
    {
        $svg = self::generateSvgPuml('puml.interface', compact('interface'));
        $title = $interface->application->name . ': ' . $interface->name;
        return view('overview', compact('svg', 'title'));
    }

    /**
     * @param Application $application
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function application(Application $application)
    {
        $svg = self::generateSvgPuml('puml.application', compact('application'));
        $title = $application->name;
        return view('overview', compact('svg', 'title'));
    }

    /**
     * @param Database $database
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function database(Database $database)
    {
        $svg = self::generateSvgPuml('puml.database', compact('database'));
        $title = $database->name;
        return view('overview', compact('svg', 'title'));
    }

    /**
     * Render a specefic view as puml to generate a svg image
     *
     * @param string $viewName
     * @param array $viewData
     *
     * @return string
     * @throws \Throwable
     */
    private static function generateSvgPuml(string $viewName, array $viewData): string
    {
        $pumlSource = view($viewName, $viewData)->render();
        if (Cache::has($pumlSource)) {
            return Cache::get($pumlSource);
        }

        $tmpFile = tmpfile();
        $tmpFilePath = stream_get_meta_data($tmpFile)['uri'];
        file_put_contents($tmpFilePath, view($viewName, $viewData)->render());

        $cmd = sprintf(
            'java -jar %s -charset UTF-8 -tsvg %s',
            env('PLANTUML_JAR', '/opt/plantuml.jar'),
            $tmpFilePath
        );
        exec($cmd, $output);
        $svgFilePath = explode('.', $tmpFilePath)[0] . '.svg';
        $svg = file_get_contents($svgFilePath);
        unlink($svgFilePath);

        Cache::put($pumlSource, $svg, 60 * 60 * 24 * 14);

        return $svg;
    }
}
