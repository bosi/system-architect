<?php

namespace App\Http\Controllers;

use App\Models\Property;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
    public const VIEW_PREFIX = 'property';
    public const ROUTE_PREFIX = 'properties';
    public const TITLE_PREFIX = 'labels.titles.' . self::VIEW_PREFIX;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = Property::all();
        $title = __(self::TITLE_PREFIX . '.index');
        return view(self::VIEW_PREFIX . '.index', compact('title', 'properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = __(self::TITLE_PREFIX . '.create');
        return view(self::VIEW_PREFIX . '.createOrUpdate', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $property = new Property($request->except(['_token']));
        $property->saveOrFail();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-created', ['type' => 'Property']));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Property $property
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Property $property
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Property $property)
    {
        $title = __(self::TITLE_PREFIX . '.update');
        return view(self::VIEW_PREFIX . '.createOrUpdate', compact('title', 'property'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Property     $property
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Property $property)
    {
        $property->fill($request->except(['_token']));
        if (!$request->has('is_enum')) {
            $property->is_enum = false;
        }
        $property->saveOrFail();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-updated', ['type' => 'Property']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Property $property
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property)
    {
        $property->delete();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-deleted', ['type' => 'Property']));
    }
}
