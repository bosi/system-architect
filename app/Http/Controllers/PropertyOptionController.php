<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\PropertyOption;
use Illuminate\Http\Request;

class PropertyOptionController extends Controller
{
    public const VIEW_PREFIX = 'property-option';
    public const ROUTE_PREFIX = 'property-options';
    public const TITLE_PREFIX = 'labels.titles.' . self::VIEW_PREFIX;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $propertyOptions = PropertyOption::allSorted();
        $title = __(self::TITLE_PREFIX . '.index');
        return view(self::VIEW_PREFIX . '.index', compact('title', 'propertyOptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $properties = [];
        foreach (Property::query()->where('is_enum', true)->get() as $property) {
            $properties[$property->id] = $property->name;
        }

        $title = __(self::TITLE_PREFIX . '.create');
        return view(self::VIEW_PREFIX . '.createOrUpdate', compact('title', 'properties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $propertyOption = new PropertyOption($request->except(['_token']));
        $propertyOption->saveOrFail();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-created', ['type' => 'Property-Option']));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\PropertyOption $propertyOption
     *
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyOption $propertyOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\PropertyOption $propertyOption
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(PropertyOption $propertyOption)
    {
        $properties = [];
        foreach (Property::all() as $property) {
            $properties[$property->id] = $property->name;
        }

        $title = __(self::TITLE_PREFIX . '.update');
        return view(self::VIEW_PREFIX . '.createOrUpdate', compact('title', 'propertyOption', 'properties'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request   $request
     * @param \App\Models\PropertyOption $propertyOption
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update(Request $request, PropertyOption $propertyOption)
    {
        $propertyOption->fill($request->except(['_token']));
        $propertyOption->saveOrFail();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-updated', ['type' => 'Property-Option']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\PropertyOption $propertyOption
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(PropertyOption $propertyOption)
    {
        $propertyOption->delete();
        return redirect()
            ->route(self::ROUTE_PREFIX . '.index')
            ->with('success', __('labels.messages.entry-deleted', ['type' => 'Property-Option']));
    }
}
