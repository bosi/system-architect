<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Application extends Model
{
    use HasFactory;

    public const LIFECYCLE_STATUS_PLANNED = 1;
    public const LIFECYCLE_STATUS_IN_USE = 10;
    public const LIFECYCLE_STATUS_TO_BE_KILLED = -1;

    public const LIFECYCLE_STATUS = [
        self::LIFECYCLE_STATUS_PLANNED      => 'planned',
        self::LIFECYCLE_STATUS_IN_USE       => 'in use',
        self::LIFECYCLE_STATUS_TO_BE_KILLED => 'to be killed',
    ];

    protected $fillable = ['name', 'description', 'lifecycle_status', 'is_user_application'];

    /**
     * Get all applications sorted
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function allSorted(Builder $baseQuery = null)
    {
        return ($baseQuery ?? self::query())
            ->orderBy('name')
            ->orderBy('description')
            ->get();
    }

    /**
     * Get the identifier of the puml element
     *
     * @return string
     */
    public function getPumlName()
    {
        return 'app_' . $this->id;
    }

    /**
     * Get the displayed title of the puml element
     *
     * @return string
     */
    public function getPumlTitle(): string
    {
        $before = '<b><u>';
        $after = ' </u></b>';

        return $before . chunk_split(strip_tags($this->getTitle()), 35, $after . '\n' . $before) . $after;
    }

    /**
     * Get the displayed title for forms (max-length is 100 characters)
     *
     * @return string
     */
    public function getTitle(): string
    {
        $title = Str::limit($this->name, 30);

        if (!empty($this->description)) {
            $title .= ' | ' . Str::limit($this->description, 100);
        }

        return $title;
    }

    /**
     * Get background color based on type
     *
     * @return string
     */
    public function getPumlBackgroundColor(): string
    {
        switch ($this->lifecycle_status) {
            case self::LIFECYCLE_STATUS_PLANNED:
                return '#57BF45';
            case self::LIFECYCLE_STATUS_TO_BE_KILLED:
                return '#FF6765';
            default:
                return '';
        }
    }

    /**
     * Get name of the group for grouped puml diagram
     *
     * @param Property $groupProperty
     *
     * @return string
     */
    public function getPumlGroupName(Property $groupProperty): string
    {
        $applicationProperty = $this
            ->application_properties()
            ->where(['property_id' => $groupProperty->id])
            ->first();
        return ($applicationProperty ?? new \App\Models\ApplicationProperty())->getPumlValue();
    }

    /** ##### Relations ##### */

    public function application_interfaces()
    {
        return $this->hasMany(ApplicationInterface::class);
    }

    public function usages()
    {
        return $this->hasMany(Usage::class);
    }

    public function database_connections()
    {
        return $this->hasMany(DatabaseConnection::class);
    }

    public function application_properties()
    {
        return $this->hasMany(ApplicationProperty::class);
    }
}
