<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ApplicationInterface extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'application_id'];

    /**
     * Get the identifier of the puml element
     *
     * @return string
     */
    public function getPumlName()
    {
        return 'app_' . $this->application_id . '_interface_' . $this->id;
    }

    /**
     * Get the displayed title of the puml element
     *
     * @return mixed|string
     */
    public function getPumlTitle(): string
    {
        return Str::limit(chunk_split($this->name, 20, '\n'), 40);
    }

    /**
     * Get all app-interfaces order by its applications
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function allSorted()
    {
        return self::query()
            ->join('applications', 'applications.id', '=', 'application_interfaces.application_id')
            ->select(['application_interfaces.*'])
            ->orderBy('applications.name')
            ->orderBy('applications.description')
            ->orderBy('application_interfaces.name')
            ->get();
    }

    /** ##### Relations ##### */

    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    public function usages()
    {
        return $this->hasMany(Usage::class);
    }
}
