<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationProperty extends Model
{
    use HasFactory;

    protected $fillable = ['application_id', 'property_id', 'value', 'property_option_id'];

    /**
     * @inheritDoc
     */
    protected function setKeysForSaveQuery($query)
    {
        $query
            ->where('application_id', '=', $this->getAttribute('application_id'))
            ->where('property_id', '=', $this->getAttribute('property_id'));
        return $query;
    }

    /**
     * Get the displayed title of the puml element
     *
     * @return string
     */
    public function getPumlTitle(): string
    {
        return $this->property->name;
    }

    /**
     * Get the displayed value of the puml element
     *
     * @return string
     */
    public function getPumlValue(): string
    {
        return $this->property->is_enum ?? false ? $this->property_option->name ?? 'undefined' : $this->value ?? 'undefined';
    }

    /** ##### Relations ##### */

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function property_option()
    {
        return $this->belongsTo(PropertyOption::class);
    }
}
