<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Database extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'type'];

    /**
     * Get the identifier of the puml element
     *
     * @return string
     */
    public function getPumlName()
    {
        return 'db_' . $this->id;
    }

    /**
     * Get name of the group for grouped puml diagram
     *
     * @param Property $groupProperty
     *
     * @return string
     */
    public function getPumlGroupName(Property $groupProperty): string
    {
        $possibleGroups = [];

        foreach ($this->database_connections as $databaseConnection) {
            $groupName = $databaseConnection->application->getPumlGroupName($groupProperty);
            if ($groupName === 'undefined') {
                continue;
            }

            if (!array_key_exists($groupName, $possibleGroups)) {
                $possibleGroups[$groupName] = 0;
            }

            $possibleGroups[$groupName]++;
        }


        if (empty($possibleGroups)) {
            return 'undefined';
        }

        return array_keys($possibleGroups, max($possibleGroups))[0];
    }

    /** ##### Relations ##### */

    public function database_connections()
    {
        return $this->hasMany(DatabaseConnection::class);
    }
}
