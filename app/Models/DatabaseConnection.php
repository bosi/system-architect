<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class DatabaseConnection extends Model
{
    protected $fillable = ['application_id', 'database_id'];
    public $timestamps = false;

    /**
     * @inheritDoc
     */
    protected function setKeysForSaveQuery($query)
    {
        $query
            ->where('application_id', '=', $this->getAttribute('application_id'))
            ->where('database_id', '=', $this->getAttribute('database_id'));
        return $query;
    }

    /** ##### Relations ##### */

    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    public function database()
    {
        return $this->belongsTo(Database::class);
    }
}
