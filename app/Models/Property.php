<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'is_enum', 'description'];

    /**
     * Get all available options for this property
     * (you may check is_enum before to ensure options are necessary)
     *
     * @return array
     */
    public function getOptions(): array
    {
        $options = ['' => ''];
        foreach ($this->propertyOptions ?? [] as $propertyOption) {
            $options[$propertyOption->id] = $propertyOption->name;
        }

        return $options;
    }

    /** ##### Relations ##### */

    public function applicationProperties()
    {
        return $this->hasMany(ApplicationProperty::class);
    }

    public function propertyOptions()
    {
        return $this->hasMany(PropertyOption::class);
    }
}
