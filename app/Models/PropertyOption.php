<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyOption extends Model
{
    use HasFactory;

    protected $fillable = ['property_id', 'name', 'color', 'description'];

    protected static function boot()
    {
        parent::boot();

        self::saving(function (self $propOpt) {
            if (empty($propOpt->color) || $propOpt->color === '#000000') {
                $propOpt->color = null;
            }
        });
    }

    /** ##### Relations ##### */

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * Get all property-options order by its properties
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function allSorted()
    {
        return self::query()
            ->join('properties', 'properties.id', '=', 'property_options.property_id')
            ->select(['property_options.*'])
            ->orderBy('properties.name')
            ->orderBy('property_options.name')
            ->get();
    }
}
