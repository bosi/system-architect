<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usage extends Model
{
    use HasFactory;

    protected $fillable = ['application_id', 'application_interface_id'];
    public $timestamps = false;

    /**
     * @inheritDoc
     */
    protected function setKeysForSaveQuery($query)
    {
        $query
            ->where('application_id', '=', $this->getAttribute('application_id'))
            ->where('application_interface_id', '=', $this->getAttribute('application_interface_id'));
        return $query;
    }

    /** ##### Relations ##### */

    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    public function application_interface()
    {
        return $this->belongsTo(ApplicationInterface::class);
    }
}
