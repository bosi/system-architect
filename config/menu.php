<?php

return [
    'side_nav' => [
        'items' => [
            [
                'route' => 'graphic.index',
                'label' => 'labels.menu.overview',
                'icon'  => 'fa-sitemap',
            ],
            [
                'route' => 'graphic.matrix',
                'label' => 'labels.menu.matrix',
                'icon'  => 'fa-border-all',
            ],
            [
                'route' => 'graphic.table',
                'label' => 'labels.menu.table',
                'icon'  => 'fa-table',
            ],
            [
                'route' => 'applications.index',
                'label' => 'labels.menu.applications',
                'icon'  => 'fa-server',
            ],
            [
                'route' => 'databases.index',
                'label' => 'labels.menu.databases',
                'icon'  => 'fa-database',
            ],
            [
                'route' => 'application-interfaces.index',
                'label' => 'labels.menu.application-interfaces',
                'icon'  => 'fa-door-open',
            ],
            [
                'route' => 'properties.index',
                'label' => 'labels.menu.properties',
                'icon'  => 'fa-list',
            ],
            [
                'route' => 'property-options.index',
                'label' => 'labels.menu.property-options',
                'icon'  => 'fa-list',
            ],
        ],
    ],
];