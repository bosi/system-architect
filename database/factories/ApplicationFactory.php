<?php

namespace Database\Factories;

use App\Models\Application;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Application>
 */
class ApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
        'name'             => $this->faker->name,
        'lifecycle_status' => $this->faker->randomElement(array_keys(Application::LIFECYCLE_STATUS)),
        'description'      => $this->faker->sentence,
    ];
    }
}
