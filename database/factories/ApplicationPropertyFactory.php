<?php

namespace Database\Factories;

use App\Models\Application;
use App\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ApplicationProperty>
 */
class ApplicationPropertyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'value'          => $this->faker->name,
            'application_id' => fn() => Application::factory()->create()->id,
            'property_id'    => fn() => Property::factory()->create()->id,
        ];
    }
}
