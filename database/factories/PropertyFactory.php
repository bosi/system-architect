<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Property>
 */
class PropertyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name'    => $this->faker->name,
            'is_enum' => $this->faker->randomElement([0, 1]),
        ];
    }

    public function enum()
    {
        return $this->state(function (array $attributes) {
            return ['is_enum' => 1];
        });
    }
}
