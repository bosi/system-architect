<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usages', function (Blueprint $table) {
            $table->unsignedInteger('application_id');
            $table->unsignedInteger('app_interface_id');
            $table->primary(['application_id', 'app_interface_id']);
            $table->foreign(['application_id'])->references('id')->on('applications');
            $table->foreign(['app_interface_id'])->references('id')->on('app_interfaces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usages');
    }
}
