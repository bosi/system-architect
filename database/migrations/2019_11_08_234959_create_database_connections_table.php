<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatabaseConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('database_connections', function (Blueprint $table) {
            $table->unsignedInteger('application_id');
            $table->unsignedInteger('database_id');
            $table->primary(['application_id', 'database_id']);
            $table->foreign(['application_id'])->references('id')->on('applications');
            $table->foreign(['database_id'])->references('id')->on('databases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('database_connections');
    }
}
