<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_properties', function (Blueprint $table) {
            $table->unsignedInteger('application_id');
            $table->unsignedInteger('property_id');
            $table->primary(['application_id', 'property_id']);
            $table->foreign(['application_id'])->references('id')->on('applications');
            $table->foreign(['property_id'])->references('id')->on('properties');

            $table->string('value')->nullable();
            $table->unsignedInteger('property_option_id')->nullable();
            $table->foreign(['property_option_id'])->references('id')->on('property_options');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_properties');
    }
}
