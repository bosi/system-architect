<?php

use App\Models\ApplicationInterface;
use App\Models\Application;
use App\Models\ApplicationProperty;
use App\Models\Database;
use App\Models\DatabaseConnection;
use App\Models\Property;
use App\Models\PropertyOption;
use App\Models\Usage;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        ($p1 = new Property(['name' => 'URL']))->save();
        ($p2 = new Property(['name' => 'Language', 'is_enum' => true]))->save();
        ($p3 = new Property(['name' => 'Responsible team', 'is_enum' => true]))->save();

        ($po21 = new PropertyOption(['property_id' => $p2->id, 'name' => 'PHP7.1']))->save();
        ($po22 = new PropertyOption(['property_id' => $p2->id, 'name' => 'PHP7.2']))->save();
        ($po23 = new PropertyOption(['property_id' => $p2->id, 'name' => 'Go1.13']))->save();

        ($po31 = new PropertyOption(['property_id' => $p3->id, 'name' => 'Team Ninja']))->save();
        ($po32 = new PropertyOption(['property_id' => $p3->id, 'name' => 'Team Knight']))->save();

        ($app1 = new Application(['name' => 'Testapp 1']))->save();
        ($appIf11 = new ApplicationInterface(['name' => 'HTTP (Webinterface)', 'application_id' => $app1->id]))->save();
        (new ApplicationProperty(['application_id' => $app1->id, 'property_id' => $p1->id, 'value' => 'http://example.com']))->save();
        (new ApplicationProperty(['application_id' => $app1->id, 'property_id' => $p2->id, 'property_option_id' => $po21->id]))->save();
        (new ApplicationProperty(['application_id' => $app1->id, 'property_id' => $p3->id, 'property_option_id' => $po31->id]))->save();

        ($app2 = new Application(['name' => 'Testapp 2']))->save();
        ($appIf21 = new ApplicationInterface(['name' => 'HTTP (API)', 'application_id' => $app2->id]))->save();
        (new ApplicationProperty(['application_id' => $app2->id, 'property_id' => $p1->id, 'value' => 'http://example.com']))->save();
        (new ApplicationProperty(['application_id' => $app2->id, 'property_id' => $p2->id, 'property_option_id' => $po22->id]))->save();
        (new ApplicationProperty(['application_id' => $app2->id, 'property_id' => $p3->id, 'property_option_id' => $po31->id]))->save();

        ($app3 = new Application(['name' => 'Testapp 3']))->save();
        ($appIf31 = new ApplicationInterface(['name' => 'HTTP (Webinterface)', 'application_id' => $app3->id]))->save();
        ($appIf32 = new ApplicationInterface(['name' => 'HTTP (API)', 'application_id' => $app3->id]))->save();
        (new ApplicationProperty(['application_id' => $app3->id, 'property_id' => $p1->id, 'value' => 'http://example.com']))->save();
        (new ApplicationProperty(['application_id' => $app3->id, 'property_id' => $p2->id, 'property_option_id' => $po23->id]))->save();
        (new ApplicationProperty(['application_id' => $app3->id, 'property_id' => $p3->id, 'property_option_id' => $po32->id]))->save();

        (new Usage(['application_id' => $app1->id, 'application_interface_id' => $appIf21->id]))->save();

        ($db1 = new Database(['name' => 'Test DB']))->save();

        (new DatabaseConnection(['application_id' => $app1->id, 'database_id' => $db1->id]))->save();
        (new DatabaseConnection(['application_id' => $app3->id, 'database_id' => $db1->id]))->save();

        ($app4 = new Application(['name' => 'Browser', 'is_user_application' => true]))->save();
        (new Usage(['application_id' => $app4->id, 'application_interface_id' => $appIf11->id]))->save();
        (new Usage(['application_id' => $app4->id, 'application_interface_id' => $appIf31->id]))->save();

        ($app5 = new Application(['name' => 'Local client', 'is_user_application' => true]))->save();
        (new Usage(['application_id' => $app5->id, 'application_interface_id' => $appIf32->id]))->save();
    }
}
