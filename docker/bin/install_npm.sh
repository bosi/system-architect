#!/bin/bash

set -eu

curl -sL "https://deb.nodesource.com/setup_lts.x" | bash -
apt-get update
apt-get install -y nodejs node-less
