#!/bin/bash

apt-get update && apt-get install -y \
    mariadb-client \
    git \
    vim \
    procps \
    zip \
    unzip \
    cron \
    wget \
    sudo \
    libicu-dev \
    libzip-dev zlib1g-dev \
    --no-install-recommends
        
docker-php-ext-install pdo_mysql
docker-php-ext-configure zip --with-libzip
docker-php-ext-install zip
docker-php-ext-install exif
docker-php-ext-install intl
docker-php-ext-install mysqli