#!/bin/bash

PLANTUML_VERSION=1.2019.6

apt-get install -y graphviz wget ca-certificates curl
wget -q "http://downloads.sourceforge.net/project/plantuml/${PLANTUML_VERSION}/plantuml.${PLANTUML_VERSION}.jar" -O /opt/plantuml.jar