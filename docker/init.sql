
CREATE DATABASE IF NOT EXISTS `system_architect` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `system_architect`.* TO 'system_architect'@'%' ;

CREATE DATABASE IF NOT EXISTS `system_architect_test` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `system_architect_test`.* TO 'system_architect'@'%' ;
