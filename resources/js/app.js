require('./bootstrap');

import DataTable from './components/DataTable'
import Vue from 'vue';
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'

window.Vue = require('vue');

Vue.component('data-table', DataTable)

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-multiselect/dist/vue-multiselect.min.css'

const app = new Vue({
    el: '#app',
});
