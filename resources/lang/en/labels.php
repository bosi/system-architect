<?php

return [
    'menu'     => [
        'overview'               => 'Overview',
        'matrix'                 => 'Matrix-Overview',
        'table'                  => 'Table-Overview',
        'applications'           => 'Applications',
        'databases'              => 'Databases',
        'application-interfaces' => 'Application-Interfaces',
        'properties'             => 'Properties',
        'property-options'       => 'Property-Options',
    ],
    'titles'   => [
        'application'           => [
            'index'  => 'Applications',
            'create' => 'Create Application',
            'update' => 'Edit Application',
        ],
        'database'              => [
            'index'  => 'Databases',
            'create' => 'Create Database',
            'update' => 'Edit Database',
        ],
        'application-interface' => [
            'index'  => 'Application-Interfaces',
            'create' => 'Create Application-Interface',
            'update' => 'Edit Application-Interface',
        ],
        'property'              => [
            'index'  => 'Properties',
            'create' => 'Create Property',
            'update' => 'Edit Property',
        ],
        'property-option'       => [
            'index'  => 'Property-Options',
            'create' => 'Create Property-Option',
            'update' => 'Edit Property-Option',
        ],
    ],
    'buttons'  => [
        'add_new' => 'Add new',
    ],
    'messages' => [
        'entry-created' => ':type was created',
        'entry-updated' => ':type was updated',
        'entry-deleted' => ':type was deleted',
    ]
];