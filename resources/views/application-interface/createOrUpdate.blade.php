@extends('layout')

@section('content')
    {{ Aire::resourceful($applicationInterface ?? new \App\Models\ApplicationInterface(), 'application-interfaces') }}
    {{ Aire::input('name', 'Name') }}
    {{ Aire::select($applications, 'application_id', 'Application') }}
    {{ Aire::submit() }}
    {{ Aire::close() }}
@endsection
