@extends('layout')

@section('content')
    @include('components.button-create', ['route' => route('application-interfaces.create')])
    <br><br>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Application</th>
            <th>Interface name</th>
            <th></th>
            <th></th>
        </tr>
        @foreach($appInterfaces as $appInterface)
            <tr id="entry-{{ $appInterface->id }}">
                <th>{{ $appInterface->id }}</th>
                <td>{{ $appInterface->application->getTitle() }}</td>
                <td>{{ $appInterface->name }}</td>
                <td>@include('components.button-edit', ['route' => route('application-interfaces.edit', ['application_interface'=>$appInterface])])</td>
                <td>@include('components.button-delete', ['route' => route('application-interfaces.destroy', ['application_interface'=>$appInterface])])</td>
            </tr>
        @endforeach
    </table>
@endsection
