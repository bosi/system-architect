@extends('layout')

@section('content')
    {{ Aire::resourceful($application ?? new \App\Models\Application()) }}

    @php($isUserApp = $application->is_user_application ?? request('user_app') ?? 0)
    {{ Aire::hidden('is_user_application', $isUserApp)->defaultValue(0) }}
    {{ Aire::input('name', 'Name') }}

    @if($isUserApp)
        @php($title .= ' (User)')
        {{ Aire::hidden('description', 'User-Application') }}
        {{ Aire::hidden('lifecycle_status', \App\Models\Application::LIFECYCLE_STATUS_IN_USE) }}
    @else
        {{ Aire::input('description', 'Description') }}
        {{ Aire::select(\App\Models\Application::LIFECYCLE_STATUS, 'lifecycle_status', 'Lifecycle Status') }}

        @foreach($applicationProperties as $applicationProperty)
            @if($applicationProperty['is_enum']??false)
                {{ Aire::select(
                    $applicationProperty['options'],
                    'applicationProperties['.$applicationProperty['property_id'].']',
                    $applicationProperty['name']
                )->value($applicationProperty['value'] ?? '') }}
            @else
                {{ Aire::input(
                    'applicationProperties['.$applicationProperty['property_id'].']',
                    $applicationProperty['name']
                )->value($applicationProperty['value'] ?? '') }}
            @endif
        @endforeach
    @endif

    <h3>Usages</h3>
    @php($options=[])
    @php($values=[])
    @php(array_map(function ($v)use (&$options, &$values){$options[$v['target_application_interface_id']]=$v['name']; if ($v['value']??false){$values[]=$v['target_application_interface_id'];}},$usages ))

    {{ Aire::select($options, 'usages')
        ->multiple()
        ->value($values)
        ->style('height: ' . min(count($usages)*25, 300) . 'px;') }}

    {{ Aire::submit() }}

    {{ Aire::close() }}
@endsection
