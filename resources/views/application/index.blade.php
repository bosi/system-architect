@extends('layout')

@section('content')
    @include('components.button-create', ['route' => route('applications.create')])
    @include('components.button-create', ['route' => route('applications.create', ['user_app' => true]), 'label' => __('labels.buttons.add_new').' User-Application'])
    <br><br>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Lifecycle Status</th>
            <th></th>
            <th></th>
        </tr>
        @foreach($applications as $application)
            <tr id="entry-{{ $application->id }}">
                <th>{{ $application->id }}</th>
                <td>{{ $application->name }}</td>
                <td>{{ $application->description }}</td>
                <td style="min-width: 130px">
                    <span class="dot" style="background-color: {{ $application-> getPumlBackgroundColor() }}"></span>
                    {{ \App\Models\Application::LIFECYCLE_STATUS[$application->lifecycle_status] }}
                </td>
                <td>@include('components.button-edit', ['route' => route('applications.edit', compact('application'))])</td>
                <td>@include('components.button-delete', ['route' => route('applications.destroy', compact('application'))])</td>
            </tr>
        @endforeach
    </table>
@endsection
