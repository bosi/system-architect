<a href="{{ $route }}">
    <button class="btn btn-primary"><i class="fa fa-plus-circle"></i> {{ $label ?? __('labels.buttons.add_new') }}</button>
</a>