<div class="button-form">
    <form method="POST" action="{{ $route }}">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger btn-sm">
            <i class="fa fa-trash"></i> delete
        </button>
    </form>
</div>
