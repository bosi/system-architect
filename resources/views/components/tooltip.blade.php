@if(!empty($text ?? ''))
    <span class="info">
        <i class="fa fa-info"></i>
        <span class="infotext">{{ $text }}</span>
    </span>
@endif