@extends('layout')

@section('content')
    {{ Aire::resourceful($database ?? new \App\Models\Database()) }}
    {{ Aire::input('name', 'Name') }}
    {{ Aire::input('description', 'Description') }}
    {{ Aire::input('type', 'Type') }}

    @php($options=[])
    @php($values=[])
    @php(array_map(function ($v)use (&$options, &$values){$options[$v['target_application_id']]=$v['name']; if ($v['value']??false){$values[]=$v['target_application_id'];}},$applications ))

    {{ Aire::select($options, 'usages')
        ->multiple()
        ->value($values)
        ->style('height: ' . min(count($applications)*25, 300) . 'px;') }}

    {{ Aire::submit() }}
    {{ Aire::close() }}
@endsection
