@extends('layout')

@section('content')
    @include('components.button-create', ['route' => route('databases.create')])
    <br><br>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Type</th>
            <th></th>
            <th></th>
        </tr>
        @foreach($databases as $database)
            <tr id="entry-{{ $database->id }}">
                <th>{{ $database->id }}</th>
                <td>{{ $database->name }}</td>
                <td>{{ $database->description }}</td>
                <td>{{ $database->type }}</td>
                <td>@include('components.button-edit', ['route' => route('databases.edit', compact('database'))])</td>
                <td>@include('components.button-delete', ['route' => route('databases.destroy', compact('database'))])</td>
            </tr>
        @endforeach
    </table>
@endsection
