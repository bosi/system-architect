<html>
<head>
    <title>System Architect - {{ $title }}</title>

    <link href={{ asset("assets/css/bootstrap.min.css") }} rel="stylesheet"/>
    <link href={{ asset("assets/css/fontawesome-all.css") }} rel="stylesheet"/>
    <link href={{ asset("assets/css/app.css") }} rel="stylesheet"/>

    <link rel="shortcut icon" href="{{ asset('assets/images/logo_without_text.png') }}" type="image/png">
</head>
</html>
<body>

<div class="container-fluid" id="base">
    <div class="row">
        <nav class="nav flex-column col-2 shadow-box">
            <img src="{{ asset("assets/images/logo.png") }}" alt="Logo" id="logo">
            @section('menu')
                <ul class="navbar-nav">
                    @foreach(config('menu.side_nav.items') as $navItem)
                        @php($route = route($navItem['route']))
                        <li class="nav-item @if(\App\Helpers\MenuHelper::isActive($route)) active @endif">
                            <a href="{{ $route }}" class="nav-link">
                                <i class="fa fa-lg {{ $navItem['icon'] }}"></i> {{ __($navItem['label']) }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @show
        </nav>
        <main class="col-10">
            <div id="title">
                <h1>{{ $title }}</h1>
            </div>
            <br>
            <div id="content" class="shadow-box">
                @include('messages')

                @yield('content')
            </div>
        </main>
    </div>
</div>

<script type="text/javascript" src="{{ asset("js/app.js") }}"></script>

</body>
