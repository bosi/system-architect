<?php
/** @var \App\Helpers\Matrix $matrix */
?>

@extends('layout')

@section('content')
    {{ Aire::open()->route('graphic.matrix')->addClass('inline')->get() }}
    {{ Aire::select(\App\Helpers\Matrix::getPropertyList(), 'prop_id_column', 'Columns:')->value(request()->get('prop_id_column')) }}
    {{ Aire::select(\App\Helpers\Matrix::getPropertyList(), 'prop_id_row', 'Rows:')->value(request()->get('prop_id_row')) }}
    {{ Aire::select(\App\Helpers\Matrix::getPropertyList(), 'prop_id_color', 'Color:')->value(request()->get('prop_id_color')) }}
    {{ Aire::submit('Show') }}
    {{ Aire::close() }}

    @if($matrix->getPropColor() !== null)
        <div class="legend">
            @if(count($matrix->getColorValues()) > 0)
                <b>
                    {{ $matrix->getPropColor()->name }}
                    @include('components.tooltip', ['text' => $matrix->getPropColor()->description]):
                </b>
                @foreach($matrix->getColorValues() as $colorPropOpt)
                    <div class="legend-item">
                        <span class="dot" style="background-color: {{ $colorPropOpt['color'] }};"></span>
                        {{ $colorPropOpt['name'] }}
                    </div>
                @endforeach
            @else
                <small>The selected property does not have options with colors.</small>
            @endif
        </div>
    @endif

    <table class="table table-bordered matrix">
        <tr>
            @if(count($matrix->getColumnValues()) > 1)
                @if(count($matrix->getRowValues()) > 1 )
                    <td colspan="2"
                        rowspan="{{ count($matrix->getColumnValues())>1 ? 2 : 1 }}"
                        class="bg-gray-2"></td>
                @endif

                <th colspan="{{ count($matrix->getColumnValues()) }}" class="bg-gray-2">
                    {{ $matrix->getPropColumn()->name }}
                    @include('components.tooltip', ['text' => $matrix->getPropColumn()->description])
                </th>
            @endif
        </tr>
        @if(count($matrix->getColumnValues()) > 1)
            <tr>
                @foreach($matrix->getColumnValues() as $columnName)
                    <th class="bg-gray-1">{{ $columnName === '' ?  'unset' : $columnName }}</th>
                @endforeach
            </tr>
        @endif
        @foreach($matrix->getMatrixData() as $rowName => $rowValues)
            <tr>
                @if(count($matrix->getRowValues()) > 1)
                    @if($loop->index === 0)
                        <th rowspan="{{ count($matrix->getRowValues())+1 }}" class="bg-gray-2">
                            {{ $matrix->getPropRow()->name }}
                            @include('components.tooltip', ['text' => $matrix->getPropRow()->description])
                        </th>
                    @endif
                    <th class="bg-gray-1">{{ $rowName===''? 'unset':$rowName}}</th>
                @endif
                @foreach($rowValues as $columnValues)
                    <td>
                        @foreach($columnValues as $appName)
                            <a href="{{ route('graphic.application', ['application' => $appName['id']]) }}">
                                <span class="badge badge-matrix">
                                         {{ $appName['name'] }}
                                    @if($appName['color'])
                                        <span class="dot dot-small"
                                              style="background-color: {{ $appName['color'] }}"></span>
                                    @endif
                                </span>
                            </a>
                        @endforeach
                    </td>
                @endforeach
            </tr>
        @endforeach
    </table>
@endsection
