@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <strong>Success! </strong>{{ $message }}
    </div>
@endif


@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <strong>Error! </strong>{{ $message }}
    </div>
@endif


@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block">
        <strong>Warning! </strong>{{ $message }}
    </div>
@endif


@if ($message = Session::get('info'))
    <div class="alert alert-info alert-block">
        <strong>Info! </strong>{{ $message }}
    </div>
@endif


@if ($errors->any())
    <div class="alert alert-danger">
        Please check the form below for errors
    </div>
@endif