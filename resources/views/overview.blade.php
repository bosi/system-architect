@extends('layout')

@section('content')
    @if(isset($properties) && !$properties->isEmpty())
        Group by:
        @foreach($properties as $property)
            <a href="{{ route('graphic.groupRender', ['property' => $property]) }}">{{ $property->name }}</a>@if(!$loop->last), @endif
        @endforeach
        <br><br>
    @endif

    {!! $svg !!}
@endsection
