@extends('layout')

@section('content')
    {{ Aire::resourceful($propertyOption ?? new \App\Models\PropertyOption(), 'property-options') }}
    {{ Aire::input('name', 'Name') }}
{{--    {{ Aire::input('description', 'Description') }}--}}
    {{ Aire::select($properties, 'property_id', 'Property') }}
    {{ Aire::color('color', 'Color')->defaultValue('#000000')->helpText('Set color to black to reset') }}
    {{ Aire::submit() }}
    {{ Aire::close() }}
@endsection
