@extends('layout')

@section('content')
    @include('components.button-create', ['route' => route('property-options.create')])
    <br><br>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Property name</th>
            <th>Color</th>
            <th></th>
            <th></th>
        </tr>
        @foreach($propertyOptions as $propertyOption)
            <tr id="entry-{{ $propertyOption->id }}">
                <th>{{ $propertyOption->id }}</th>
                <td>{{ $propertyOption->name }}</td>
                <td>{{ $propertyOption->property->name }}</td>
                <td>
                    @if($propertyOption->color)
                        <span class="dot" style="background-color: {{ $propertyOption->color }}"></span>
                    @else
                        -
                    @endif
                </td>
                <td>@include('components.button-edit', ['route' => route('property-options.edit', ['property_option'=> $propertyOption])])</td>
                <td>@include('components.button-delete', ['route' => route('property-options.destroy', ['property_option'=> $propertyOption])])</td>
            </tr>
        @endforeach
    </table>
@endsection
