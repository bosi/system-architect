@extends('layout')

@section('content')
    {{ Aire::resourceful($property ?? new \App\Models\Property()) }}
    {{ Aire::input('name', 'Name') }}
    {{ Aire::input('description', 'Description') }}
    {{ Aire::radioGroup([0 => 'No', 1 => 'Yes'], 'is_enum', 'Is Enum')}}
    {{ Aire::submit() }}
    {{ Aire::close() }}
@endsection
