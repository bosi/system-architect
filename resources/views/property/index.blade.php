@extends('layout')

@section('content')
    @include('components.button-create', ['route' => route('properties.create')])
    <br><br>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Is enum</th>
            <th></th>
            <th></th>
        </tr>
        @foreach($properties as $property)
            <tr id="entry-{{ $property->id }}">
                <th>{{ $property->id }}</th>
                <td>{{ $property->name }}</td>
                <td>{{ $property->is_enum ? 'yes' : 'no' }}</td>
                <td>@include('components.button-edit', ['route' => route('properties.edit', compact('property'))])</td>
                <td>@include('components.button-delete', ['route' => route('properties.destroy', compact('property'))])</td>
            </tr>
        @endforeach
    </table>
@endsection
