@startuml

' ### Styling ############################
@include('puml.style')

' ### Puml ############################
Actor User

@include('puml.templates.application')

@php($renderedApplicationIds = [])

@foreach($application->usages as $usage)
    @if(!in_array($usage->application_interface->application->id, $renderedApplicationIds))
        @include('puml.templates.application', ['application'=> $usage->application_interface->application])
        @php($renderedApplicationIds[] = $usage->application_interface->application->id)
    @endif
    @include('puml.templates.usage')
@endforeach

@foreach($application->database_connections as $databaseConnection)
    @include('puml.templates.database', ['database'=> $databaseConnection->database, 'renderDatabaseConnections'=>false])
    @include('puml.templates.databaseConnection')
@endforeach

@enduml
