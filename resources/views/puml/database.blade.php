@startuml

' ### Styling ############################
@include('puml.style')

' ### Puml ############################
@foreach($database->database_connections as $databaseConnection)
    @include('puml.templates.application', ['application' => $databaseConnection->application, 'renderInterfaces'=> false])
@endforeach

@include('puml.templates.database')

@enduml
