@startuml

' ### Styling ############################
@include('puml.style')

' ### Puml ############################
Actor User

@include('puml.templates.application', ['application' => $interface->application, 'renderInterfaces'=> false])

@foreach($interface->application->application_interfaces as $currentInterface)
    @include('puml.templates.interface', ['interface' => $currentInterface,'withLink'=> !($interface->id === $currentInterface->id)])
@endforeach

@foreach($interface->usages as $usage)
    @include('puml.templates.application', ['application'=> $usage->application, 'renderInterfaces'=> false])
    @include('puml.templates.usage')
@endforeach

@enduml
