@startuml

' ### Styling ############################
@include('puml.style')

' ### Puml ############################
@foreach($applications as $application)
    @include('puml.templates.application')
@endforeach

rectangle " " {
    Actor User

    @foreach($userApplications as $userApplication)
        @include('puml.templates.application', ['application' => $userApplication])
    @endforeach
}

@foreach($usages as $usage)
    @include('puml.templates.usage')
@endforeach

@foreach($databases as $database)
    @include('puml.templates.database')
@endforeach



@enduml
