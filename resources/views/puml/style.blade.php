left to right direction

skinparam component {
ArrowColor #424242
}

skinparam rectangle {
backgroundColor GhostWhite
borderColor DimGrey
}

skinparam database {
backgroundColor #F5ECCE
borderColor #DA8A00
}

skinparam interface {
backgroundColor GreenYellow
borderColor Green
}

' ### Functions ############################
!definelong app(name, description, url, language)
rectangle name [
<b><u>description</u></b>\n
<b>URL</b>: url
<b>Langguage</b>: language
]
!enddefinelong

!definelong appPlanned(name, description, url, language)
rectangle name #FBFC79 [
<b><u>description (planned)</u></b>\n
<b>URL</b>: url
<b>Langguage</b>: language
]
!enddefinelong

!definelong appKilled(name, description, url, language)
rectangle name #FF6765 [
<b><u>description (to be killed)</u></b>\n
<b>URL</b>: url
<b>Langguage</b>: language
]
!enddefinelong

!definelong db(name, description, type)
database name #F5ECCE [
<b><u>description</u></b>\n
<b>Type</b>: type
]
!enddefinelong
