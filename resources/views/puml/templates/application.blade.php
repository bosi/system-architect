
@if($application->is_user_application)
    rectangle {{ $application->getPumlName() }} [[{{ route('graphic.application', compact('application')) }}]] #FFE74C [
    <b>{{ $application->name }}</b>
    ]
    User -- {{ $application->getPumlName() }}
@else
    @php($groupRender = ($groupRenderProperty ?? null) !== null)

    @if($groupRender)
        package "{{ $application->getPumlGroupName($groupRenderProperty) }}" {
    @endif
    rectangle {{ $application->getPumlName() }} [[{{ route('graphic.application', compact('application')) }}]] {{ $application->getPumlBackgroundColor() }} [
        {!! $application->getPumlTitle() !!}
        @foreach($application->application_properties as $applicationProperty)
            <b>{{ $applicationProperty->getPumlTitle() }}</b>: {{ $applicationProperty->getPumlValue() }}
        @endforeach
    ]

    @if($renderInterfaces ?? true)
        @foreach($application->application_interfaces as $interface)
            @include('puml.templates.interface')
        @endforeach
    @endif

    @if($groupRender)
        }
    @endif
@endif
