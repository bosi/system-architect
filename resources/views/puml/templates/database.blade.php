@php($groupRender = ($groupRenderProperty ?? null) !== null)

@if($groupRender)
    package "{{ $database->getPumlGroupName($groupRenderProperty) }}" {
@endif

database {{ $database->getPumlName() }} [[{{ route('graphic.database', compact('database')) }}]] #F5ECCE [
    <b><u>{{ $database->description ?? $database->name }}</u></b>\n
    @if(!empty($database->type))
        <b>Type</b>: {{ $database->type }}
    @endif
]

@if($groupRender)
    }
@endif

@if($renderDatabaseConnections ?? true)
    @foreach($database->database_connections as $databaseConnection)
        @include('puml.templates.databaseConnection')
    @endforeach
@endif
