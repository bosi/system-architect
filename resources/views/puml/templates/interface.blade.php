@if($withLink ?? true)
    () "[[{{ route('graphic.interface', ['interface' => $interface]) }} {{ $interface->getPumlTitle() }}]]" as {{ $interface->getPumlName() }}
@else
    () "{{ $interface->getPumlTitle() }}" as {{ $interface->getPumlName() }}
@endif
{{ $interface->application->getPumlName() }} -up- {{ $interface->getPumlName() }}
