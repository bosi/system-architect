@extends('layout')

@section('content')
    <div id="app">
        <data-table
                :items="{{ $table->getItemsEncoded() }}"
                :fields="{{ $table->getFieldsEncoded() }}"
        ></data-table>
    </div>
@endsection