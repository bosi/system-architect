<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'GraphicController@index')->name('graphic.index');
Route::get('/matrix', 'GraphicController@matrix')->name('graphic.matrix');
Route::get('/table', 'GraphicController@table')->name('graphic.table');
Route::get('/puml/group-render/{property}', 'GraphicController@index')->name('graphic.groupRender');
Route::get('/puml/interface/{interface}', 'GraphicController@interface')->name('graphic.interface');
Route::get('/puml/application/{application}', 'GraphicController@application')->name('graphic.application');
Route::get('/puml/database/{database}', 'GraphicController@database')->name('graphic.database');

Route::resource('/applications', 'ApplicationController');
Route::resource('/databases', 'DatabaseController');
Route::resource('/application-interfaces', 'ApplicationInterfaceController');
Route::resource('/properties', 'PropertyController');
Route::resource('/property-options', 'PropertyOptionController');