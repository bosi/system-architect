<?php

namespace Step\Functional;

use Illuminate\Database\Eloquent\Model;

class CrudTester extends \FunctionalTester
{
    public const FORM_TYPE_TEXT = 'text';
    public const FORM_TYPE_SELECT = 'select';
    public const FORM_TYPE_CHECKBOX = 'checkbox';
    public const FORM_TYPE_SKIP = 'skip';

    private const FORM_FIELD_SKIP = ['id', 'created_at', 'updated_at', 'color'];

    public function checkDeleteAction(string $modelName, string $routePrefix)
    {
        $I = $this;

        $mode = $I->have($modelName);

        $I->amOnCheckedRoute($routePrefix . '.index');
        $I->click('delete', '//tr[@id="entry-' . $mode->id . '"]');
        $I->seeCurrentRouteIs($routePrefix . '.index');
        $I->dontSeeRecord($modelName, ['id' => $mode->id]);
    }

    public function checkIndexAction(string $controllerName, array $data)
    {
        $I = $this;

        $I->amOnCheckedRoute('graphic.index');
        $I->see('Overview');

        $I->click(__('labels.menu.' . $controllerName::ROUTE_PREFIX), '//nav');
        $I->seeCurrentRouteIs($controllerName::ROUTE_PREFIX . '.index');
        $I->see(__($controllerName::TITLE_PREFIX . '.index'));

        foreach ($data as $key => $value) {
            if (in_array($key, self::FORM_FIELD_SKIP)) {
                continue;
            }
            $I->see($value, '//table');
        }
    }

    public function amGoingToEditForm(Model $model, string $routePrefix): void
    {
        $I = $this;

        $I->amOnCheckedRoute($routePrefix . '.index');
        $I->click('edit', '//tr[@id="entry-' . $model->id . '"]');
        $I->seeCurrentRouteIs($routePrefix . '.edit');
    }

    public function amGoingToCreateForm(string $routePrefix, string $btnLabel = null): void
    {
        $I = $this;

        $I->amOnCheckedRoute($routePrefix . '.index');
        $I->click($btnLabel ?? __('labels.buttons.add_new'), '//button');
        $I->seeCurrentRouteIs($routePrefix . '.create');
    }

    public function checkEditForm(array $data, array $typeMap = []): void
    {
        $I = $this;

        foreach ($data as $key => $value) {
            if (in_array($key, self::FORM_FIELD_SKIP)) {
                continue;
            }

            switch ($type = $typeMap[$key] ?? self::FORM_TYPE_TEXT) {
                case self::FORM_TYPE_TEXT:
                    $I->seeInField('//input[@name="' . $key . '"]', $value);
                    break;
                case self::FORM_TYPE_SELECT;
                    $I->seeOptionIsSelected('//select[@name="' . $key . '"]', $value);
                    break;
                case self::FORM_TYPE_CHECKBOX:
                    if ($value) {
                        $I->seeCheckboxIsChecked('//input[@name="' . $key . '"]');
                    } else {
                        $I->dontSeeCheckboxIsChecked('//input[@name="' . $key . '"]');
                    }
                    break;
                case self::FORM_TYPE_SKIP:
                    break;
                default:
                    $I->fail('Type "' . $type . '" is undefined');
            }
        }
    }

    public function fillForm(array $data, array $typeMap = []): void
    {
        $I = $this;

        foreach ($data as $key => $value) {
            if (in_array($key, self::FORM_FIELD_SKIP)) {
                continue;
            }

            switch ($type = $typeMap[$key] ?? self::FORM_TYPE_TEXT) {
                case self::FORM_TYPE_TEXT:
                    $I->fillField($key, $value);
                    break;
                case self::FORM_TYPE_SELECT;
                    $I->selectOption($key, (string)$value);
                    break;
                case self::FORM_TYPE_CHECKBOX:
                    if ($value) {
                        $I->checkOption('//input[@name="' . $key . '"]');
                    } else {
                        $I->uncheckOption('//input[@name="' . $key . '"]');
                    }
                    break;
                case self::FORM_TYPE_SKIP:
                    break;
                default:
                    $I->fail('Type "' . $type . '" is undefined');
            }
        }
    }

    public function submitFormAndCheckRecord(string $modelName, string $routePrefix, array $data, array $typeMap = [])
    {
        $I = $this;

        $I->fillForm($data, $typeMap);
        $I->click('//button[@type="submit"]');

        $I->seeCurrentRouteIs($routePrefix . '.index');
        $I->seeRecord($modelName, $data);
    }
}