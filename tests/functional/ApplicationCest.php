<?php

namespace Tests\Functional;

use App\Http\Controllers\ApplicationController;
use App\Models\Application;
use Faker\Factory;
use Faker\Generator;
use Step\Functional\CrudTester;

class ApplicationCest
{
    public const FORM_TYPE_MAP = [
        'lifecycle_status'    => CrudTester::FORM_TYPE_SELECT,
        'is_user_application' => CrudTester::FORM_TYPE_SKIP,
    ];

    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function testIndex(CrudTester $I): void
    {
        $appData = $I->have(Application::class)->toArray();
        $appData['lifecycle_status'] = Application::LIFECYCLE_STATUS[$appData['lifecycle_status']];

        $I->checkIndexAction(ApplicationController::class, $appData);
    }

    public function testCreate(CrudTester $I): void
    {
        $I->amGoingToCreateForm(ApplicationController::ROUTE_PREFIX);

        $I->submitFormAndCheckRecord(
            Application::class,
            ApplicationController::ROUTE_PREFIX,
            Application::factory()->make(['is_user_application' => false])->toArray(),
            self::FORM_TYPE_MAP
        );
    }

    public function testCreateUser(CrudTester $I): void
    {
        $I->amGoingToCreateForm(
            ApplicationController::ROUTE_PREFIX,
            __('labels.buttons.add_new') . ' User-Application'
        );

        $I->dontSeeElement('//input[@id="description"]');
        $I->dontSeeElement('//input[@id="lifecycle_status"]');

        $testName = $this->faker->name;
        $I->submitFormAndCheckRecord(Application::class, ApplicationController::ROUTE_PREFIX, ['name' => $testName]);
        $I->seeRecord(Application::class, ['name' => $testName, 'is_user_application' => true]);
    }

    public function testEdit(CrudTester $I): void
    {
        $app1 = $I->have(Application::class);

        $expect = $app1->toArray();
        $expect['lifecycle_status'] = Application::LIFECYCLE_STATUS[$expect['lifecycle_status']];

        $I->amGoingToEditForm($app1, ApplicationController::ROUTE_PREFIX);
        $I->checkEditForm($expect, self::FORM_TYPE_MAP);

        $appData = Application::factory()->make()->toArray();
        $appData['id'] = $app1->id;

        $I->submitFormAndCheckRecord(
            Application::class,
            ApplicationController::ROUTE_PREFIX,
            $appData,
            self::FORM_TYPE_MAP
        );
    }

    public function testEditUser(CrudTester $I): void
    {
        $app1 = $I->have(Application::class, ['is_user_application' => true]);

        $I->amGoingToEditForm($app1, ApplicationController::ROUTE_PREFIX);
        $I->dontSeeElement('//input[@id="description"]');
        $I->dontSeeElement('//input[@id="lifecycle_status"]');

        $testName = $this->faker->name;
        $I->checkEditForm(['name' => $app1->name]);
        $I->submitFormAndCheckRecord(Application::class, ApplicationController::ROUTE_PREFIX, ['name' => $testName]);
        $I->seeRecord(Application::class, ['name' => $testName, 'is_user_application' => true]);
    }

    public function testDelete(CrudTester $I): void
    {
        $I->checkDeleteAction(Application::class, ApplicationController::ROUTE_PREFIX);
    }
}
