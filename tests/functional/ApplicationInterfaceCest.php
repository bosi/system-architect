<?php

namespace Tests\Functional;

use App\Http\Controllers\ApplicationInterfaceController;
use App\Models\ApplicationInterface;
use Faker\Factory;
use Step\Functional\CrudTester;

class ApplicationInterfaceCest
{
    public const FORM_TYPE_MAP = ['application_id' => CrudTester::FORM_TYPE_SELECT];

    public function testIndex(CrudTester $I): void
    {
        $appIfData = ($appIf = $I->have(ApplicationInterface::class))->toArray();
        $appIfData['application_id'] = $appIf->application->getTitle();

        $I->checkIndexAction(ApplicationInterfaceController::class, $appIfData);
    }

    public function testCreate(CrudTester $I): void
    {
        $appIfData = ApplicationInterface::factory()->make()->toArray();
        $I->amGoingToCreateForm(ApplicationInterfaceController::ROUTE_PREFIX);

        $I->submitFormAndCheckRecord(
            ApplicationInterface::class,
            ApplicationInterfaceController::ROUTE_PREFIX,
            $appIfData,
            self::FORM_TYPE_MAP
        );
    }

    public function testEdit(CrudTester $I): void
    {
        $appIf = $I->have(ApplicationInterface::class);

        $expect = $appIf->toArray();
        $expect['application_id'] = $appIf->application->getTitle();

        $appData = ApplicationInterface::factory()->make()->toArray();
        $appData['id'] = $appIf->id;

        $I->amGoingToEditForm($appIf, ApplicationInterfaceController::ROUTE_PREFIX);
        $I->checkEditForm($expect, self::FORM_TYPE_MAP);

        $I->submitFormAndCheckRecord(
            ApplicationInterface::class,
            ApplicationInterfaceController::ROUTE_PREFIX,
            $appData,
            self::FORM_TYPE_MAP
        );
    }

    public function testDelete(CrudTester $I): void
    {
        $I->checkDeleteAction(ApplicationInterface::class, ApplicationInterfaceController::ROUTE_PREFIX);
    }
}
