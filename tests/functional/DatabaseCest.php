<?php

namespace Tests\Functional;

use App\Http\Controllers\DatabaseController;
use App\Models\Database;
use Faker\Factory;
use Step\Functional\CrudTester;

class DatabaseCest
{
    public function testIndex(CrudTester $I): void
    {
        $I->checkIndexAction(DatabaseController::class, $I->have(Database::class)->toArray());
    }

    public function testCreate(CrudTester $I): void
    {
        $I->amGoingToCreateForm(DatabaseController::ROUTE_PREFIX);

        $I->submitFormAndCheckRecord(
            Database::class,
            DatabaseController::ROUTE_PREFIX,
            Database::factory()->make()->toArray(),
            );
    }

    public function testEdit(CrudTester $I): void
    {
        $database = $I->have(Database::class);

        $I->amGoingToEditForm($database, DatabaseController::ROUTE_PREFIX);
        $I->checkEditForm($database->toArray());

        $databaseData = Database::factory()->make()->toArray();
        $databaseData['id'] = $database->id;

        $I->submitFormAndCheckRecord(
            Database::class,
            DatabaseController::ROUTE_PREFIX,
            $databaseData
        );
    }

    public function testDelete(CrudTester $I): void
    {
        $I->checkDeleteAction(Database::class, DatabaseController::ROUTE_PREFIX);
    }
}
