<?php

namespace Tests\Functional;

use App\Models\Application;
use App\Models\ApplicationInterface;
use App\Models\ApplicationProperty;
use App\Models\Database;
use App\Models\Property;
use App\Models\PropertyOption;
use FunctionalTester;

class GraphicCest
{
    public function testIndex(FunctionalTester $I): void
    {
        $property1 = $I->have(Property::class, ['is_enum' => false]);
        $property2 = $I->have(Property::class, ['is_enum' => true]);
        $propertyOption = $I->have(PropertyOption::class, ['property_id' => $property2->id]);

        $app1 = $I->have(Application::class);
        $app2 = $I->have(Application::class);
        $userApp2 = $I->have(Application::class, ['is_user_application' => true]);
        $appIf1 = $I->have(ApplicationInterface::class, ['application_id' => $app1->id]);
        $appProperty1 = $I->have(ApplicationProperty::class,
            [
                'application_id' => $app1->id,
                'property_id'    => $property1->id,
            ]
        );

        $appProperty2 = $I->have(ApplicationProperty::class,
            [
                'application_id'     => $app2->id,
                'property_id'        => $property2->id,
                'property_option_id' => $propertyOption->id,
            ]
        );

        $db = $I->have(Database::class);

        $I->amOnRoute('graphic.index');

        $I->seeInSource('Actor User');

        $I->seeInSource($app1->getPumlTitle());
        $I->seeInSource($app2->getPumlTitle());
        $I->seeInSource($appIf1->getPumlTitle());
        $I->seeInSource($app1->getPumlName() . ' -up- ' . $appIf1->getPumlName());
        $I->seeInSource('<b>' . $property1->name . '</b>: ' . $appProperty1->value);
        $I->seeInSource('<b>' . $property2->name . '</b>: ' . $appProperty2->property_option->name);

        $I->seeInSource($db->description);

        $I->seeInSource($userApp2->name);
        $I->seeInSource('User - - ' . $userApp2->getPumlName());
    }

    public function testMatrix(FunctionalTester $I): void
    {
        $property1 = $I->have(Property::class, ['is_enum' => false]);
        $property2 = $I->have(Property::class, ['is_enum' => true]);
        $propertyOption = $I->have(PropertyOption::class, ['property_id' => $property2->id]);

        $app1 = $I->have(Application::class);
        $app2 = $I->have(Application::class);
        $appProperty1 = $I->have(ApplicationProperty::class,
            [
                'application_id' => $app1->id,
                'property_id'    => $property1->id,
            ]
        );

        $appProperty2 = $I->have(ApplicationProperty::class,
            [
                'application_id'     => $app2->id,
                'property_id'        => $property2->id,
                'property_option_id' => $propertyOption->id,
            ]
        );

        $I->amOnRoute('graphic.matrix');

        $I->see($app1->name);
        $I->see($app2->name);

        $I->selectOption('prop_id_column', (string)$property1->id);
        $I->click('Show');
        $I->see($app1->name,'//table');
        $I->see($app2->name,'//table');
        $I->see($property1->name, '//table');
        $I->dontSee($property2->name, '//table');
        $I->see($appProperty1->getPumlValue(), '//table');
        $I->dontSee($appProperty2->getPumlValue(), '//table');

        $I->selectOption('prop_id_row', (string)$property2->id);
        $I->click('Show');
        $I->see($app1->name,'//table');
        $I->see($app2->name,'//table');
        $I->see($property1->name, '//table');
        $I->see($property2->name, '//table');
        $I->see($appProperty1->getPumlValue(), '//table');
        $I->see($appProperty2->getPumlValue(), '//table');
    }
}
