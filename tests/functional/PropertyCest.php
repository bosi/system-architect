<?php

namespace Tests\Functional;

use App\Http\Controllers\PropertyController;
use App\Models\Property;
use Faker\Factory;
use Step\Functional\CrudTester;

class PropertyCest
{
    public const FORM_TYPE_MAP = ['is_enum' => CrudTester::FORM_TYPE_SELECT];

    public function testIndex(CrudTester $I): void
    {
        $propertyData = $I->have(Property::class)->toArray();
        $propertyData['is_enum'] = $propertyData['is_enum'] ? 'yes' : 'no';

        $I->checkIndexAction(PropertyController::class, $propertyData);
    }

    public function testCreate(CrudTester $I): void
    {
        $I->amGoingToCreateForm(PropertyController::ROUTE_PREFIX);

        $I->submitFormAndCheckRecord(
            Property::class,
            PropertyController::ROUTE_PREFIX,
            Property::factory()->make()->toArray(),
            self::FORM_TYPE_MAP
        );
    }

    public function testEdit(CrudTester $I): void
    {
        $property = $I->have(Property::class);

        $expect = $property->toArray();
        unset($expect['is_enum']);

        $I->amGoingToEditForm($property, PropertyController::ROUTE_PREFIX);
        $I->checkEditForm($expect, self::FORM_TYPE_MAP);

        $appData = Property::factory()->make()->toArray();
        unset($appData['is_enum']);
        $appData['id'] = $property->id;

        $I->submitFormAndCheckRecord(
            Property::class,
            PropertyController::ROUTE_PREFIX,
            $appData,
            self::FORM_TYPE_MAP
        );
    }

    public function testDelete(CrudTester $I): void
    {
        $I->checkDeleteAction(Property::class, PropertyController::ROUTE_PREFIX);
    }
}
