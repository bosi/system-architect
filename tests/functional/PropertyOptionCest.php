<?php

namespace Tests\Functional;

use App\Http\Controllers\PropertyOptionController;
use App\Models\PropertyOption;
use Faker\Factory;
use Step\Functional\CrudTester;

class PropertyOptionCest
{
    public const FORM_TYPE_MAP = ['property_id' => CrudTester::FORM_TYPE_SELECT];

    public function testIndex(CrudTester $I): void
    {
        $optionData = ($option = $I->have(PropertyOption::class))->toArray();
        $optionData['property_id'] = $option->property->name;

        $I->checkIndexAction(PropertyOptionController::class, $optionData);
    }

    public function testCreate(CrudTester $I): void
    {
        $optionData = PropertyOption::factory()->make()->toArray();
        $I->amGoingToCreateForm(PropertyOptionController::ROUTE_PREFIX);

        $I->submitFormAndCheckRecord(
            PropertyOption::class,
            PropertyOptionController::ROUTE_PREFIX,
            $optionData,
            self::FORM_TYPE_MAP
        );
    }

    public function testEdit(CrudTester $I): void
    {
        $option = $I->have(PropertyOption::class);

        $expect = $option->toArray();
        $expect['property_id'] = $option->property->name;

        $optionData = PropertyOption::factory()->make()->toArray();
        $optionData['id'] = $option->id;

        $I->amGoingToEditForm($option, PropertyOptionController::ROUTE_PREFIX);
        $I->checkEditForm($expect, self::FORM_TYPE_MAP);

        $I->submitFormAndCheckRecord(
            PropertyOption::class,
            PropertyOptionController::ROUTE_PREFIX,
            $optionData,
            self::FORM_TYPE_MAP
        );
    }

    public function testDelete(CrudTester $I): void
    {
        $I->checkDeleteAction(PropertyOption::class, PropertyOptionController::ROUTE_PREFIX);
    }
}
