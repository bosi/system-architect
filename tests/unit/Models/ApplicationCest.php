<?php

namespace Tests\Unit\Models;


use App\Models\Application;
use App\Models\ApplicationProperty;
use App\Models\Property;
use UnitTester;

class ApplicationCest
{
    public function testGetTitle(UnitTester $I)
    {
        $rndChar = (string)random_int(0, 9);
        $rndName = str_repeat($rndChar, 30);
        $rndDescription = str_repeat($rndChar, 100);

        $application = new Application();
        $application->name = $rndName;

        $I->assertEquals($rndName, $application->getTitle());

        $application->name = $rndName . $rndChar;
        $I->assertEquals($rndName . '...', $application->getTitle());

        $application->name = $rndName;
        $application->description = $rndDescription;
        $I->assertEquals($rndName . ' | ' . $rndDescription, $application->getTitle());

        $application->name = $rndName . $rndChar;
        $I->assertEquals($rndName . '... | ' . $rndDescription, $application->getTitle());

        $application->description = $rndDescription . $rndChar;
        $I->assertEquals($rndName . '... | ' . $rndDescription . '...', $application->getTitle());

        $application->name = $rndName;
        $I->assertEquals($rndName . ' | ' . $rndDescription . '...', $application->getTitle());
    }

    public function testGetPumlTitle(UnitTester $I)
    {
        $rndChar = (string)random_int(0, 9);
        $rndName = str_repeat($rndChar, 30);
        $rndDescription = str_repeat($rndChar, 100);

        $application = new Application();
        $application->name = $rndName;
        $application->description = $rndDescription;

        $I->assertEquals(4, substr_count($application->getPumlTitle(), '</u></b>\n<b><u>'));

        $application->name = '<a>öäü';
        $application->description = '';
        $I->assertEquals('<b><u>öäü </u></b>\n<b><u> </u></b>', $application->getPumlTitle());
    }

    public function testGetPumlGroupName(UnitTester $I)
    {
        $app = $I->have(Application::class);
        $property1 = $I->have(Property::class, ['is_enum' => false]);
        $property2 = $I->have(Property::class, ['is_enum' => false]);

        $I->assertEquals('undefined', $app->getPumlGroupName($property1));
        $I->assertEquals('undefined', $app->getPumlGroupName($property2));

        $appProperty = $I->have(
            ApplicationProperty::class,
            [
                'application_id' => $app->id,
                'property_id'    => $property1->id,
            ]
        );

        $I->assertEquals($appProperty->value, $app->getPumlGroupName($property1));
        $I->assertEquals('undefined', $app->getPumlGroupName($property2));
    }
}
