<?php

namespace Tests\Unit\Models;


use App\Models\ApplicationInterface;
use UnitTester;

class ApplicationInterfaceCest
{
    public function testGetPumlTitle(UnitTester $I)
    {
        $rndChar = (string)random_int(0, 9);
        $rndName = str_repeat($rndChar, 40);

        $appIf = new ApplicationInterface();
        $appIf->name = substr($rndName, 0, 20);

        $I->assertEquals($appIf->name . '\n', $appIf->getPumlTitle());

        $appIf->name = substr($rndName, 0, 36);
        $I->assertEquals(
            substr($rndName, 0, 20) . '\n' . str_repeat($rndChar, 16) . '\n',
            $appIf->getPumlTitle()
        );

        $appIf->name = substr($rndName, 0, 38);
        $I->assertEquals(
            substr($rndName, 0, 20) . '\n' . str_repeat($rndChar, 18) . '...',
            $appIf->getPumlTitle()
        );
    }
}
