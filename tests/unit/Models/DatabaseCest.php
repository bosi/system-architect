<?php

namespace Tests\Unit\Models;


use App\Models\Application;
use App\Models\ApplicationProperty;
use App\Models\Database;
use App\Models\DatabaseConnection;
use App\Models\Property;
use UnitTester;

class DatabaseCest
{
    public function testGetPumlGroupName(UnitTester $I)
    {
        $database = $I->have(Database::class);
        $property1 = $I->have(Property::class, ['is_enum' => false]);
        $property2 = $I->have(Property::class, ['is_enum' => false]);

        $I->assertEquals('undefined', $database->getPumlGroupName($property1));
        $I->assertEquals('undefined', $database->getPumlGroupName($property2));

        $app1 = $I->have(Application::class);
        $appProperty1 = $I->have(ApplicationProperty::class, ['application_id' => $app1->id, 'property_id' => $property1->id]);
        (new DatabaseConnection(['application_id' => $app1->id, 'database_id' => $database->id]))->saveOrFail();
        $database->refresh();

        $I->assertEquals($appProperty1->value, $database->getPumlGroupName($property1));
        $I->assertEquals('undefined', $database->getPumlGroupName($property2));

        $app2 = $I->have(Application::class);
        $appProperty2 = $I->have(ApplicationProperty::class, ['application_id' => $app2->id, 'property_id' => $property1->id]);
        (new DatabaseConnection(['application_id' => $app2->id, 'database_id' => $database->id]))->saveOrFail();
        $database->refresh();
        $I->assertEquals($appProperty1->getPumlValue(), $database->getPumlGroupName($property1));

        $app3 = $I->have(Application::class);
        $I->have(ApplicationProperty::class, ['application_id' => $app3->id, 'property_id' => $property1->id, 'value' => $appProperty2->value]);
        (new DatabaseConnection(['application_id' => $app3->id, 'database_id' => $database->id]))->saveOrFail();
        $database->refresh();
        $I->assertEquals($appProperty2->getPumlValue(), $database->getPumlGroupName($property1));
    }
}
