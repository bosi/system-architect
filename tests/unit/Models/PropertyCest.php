<?php

namespace Tests\Unit\Models;


use App\Models\Property;
use App\Models\PropertyOption;
use UnitTester;

class PropertyCest
{
    public function testGetOptions(UnitTester $I)
    {
        /** @var Property $property */
        $property = $I->have(Property::class);

        $I->assertEquals(['' => ''], $property->getOptions());

        $option1 = $I->have(PropertyOption::class, ['property_id' => $property->id]);
        $option2 = $I->have(PropertyOption::class, ['property_id' => $property->id]);

        $property->refresh();

        $I->assertEquals(
            [
                ''           => '',
                $option1->id => $option1->name,
                $option2->id => $option2->name
            ],
            $property->getOptions()
        );
    }
}
